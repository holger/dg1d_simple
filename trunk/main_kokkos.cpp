// g++ -o main main.cpp -O3 -fno-tree-vectorize

#include <iostream>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <chrono>

#include "legendre.hpp"

#include "Kokkos_Core.hpp"

namespace kk = Kokkos;

int main(int argc, char** argv)
{
  std::cerr << "Test of the Kokkos discontinious Galerkin method for 1d advection starts ...\n";

  kk::initialize();

  printf("Hello World on Kokkos execution space %s\n",
         typeid(Kokkos::DefaultExecutionSpace).name());
  
  if(argc < 2) {
    std::cerr << "ERROR: argc = " << argc << " < 2 !\n";
    std::cerr << "usage: ./cours <nx> \n";
    exit(1);
  }
    
  int nx = atoi(argv[1]);
  double lx = 2*M_PI;
  double dx = lx/(nx-2);
  int nInt = 100;
  double dInt = 2./(nInt-1);
  double ddx = dx/(nInt-1);

  double cfl = 0.1;  // cfl = a*dt/dx
  double a = 1;
  double dt = cfl*dx/a;
  double T = lx/a;
  
  std::cerr << "nx = " << nx << std::endl;
  std::cerr << "lx = " << lx << std::endl;
  std::cerr << "dx = " << dx << std::endl;
  std::cerr << "cfl = " << cfl << std::endl;
  std::cerr << "a = " << a << std::endl;
  std::cerr << "dt = " << dt << std::endl;
  std::cerr << "T = " << T << std::endl;

  kk::View<double*> c0("c0", nx);
  kk::View<double*> c1("c1", nx);
  kk::View<double*> c0New("c0New", nx);
  kk::View<double*> c1New("c1New", nx);
  kk::View<double*> c0Tmp("c0Tmp", nx);
  kk::View<double*> c1Tmp("c1Tmp", nx);

  kk::View<double*> u("u", nx);
  kk::View<double*> uFine("uFine", nx*nInt);

  kk::View<double*, Kokkos::HostSpace> uRef_h("uRef", nx);
  kk::View<double*, Kokkos::HostSpace> uRefFine_h("uRefFine", nx*nInt);
  kk::View<double*, Kokkos::HostSpace> uNew("uNew", nx);
  kk::View<double*, Kokkos::HostSpace> uFineNew("uFineNew", nx*nInt);
  
  for(int i=0;i<nx*nInt;i++) {
    uRefFine_h[i] = sin(2*M_PI/((nx-2)*nInt)*(i+0.5));
  }
  
  kk::View<double*> xi("xi",3);
  kk::View<double*>::HostMirror xi_h = kk::create_mirror(xi);

  xi_h[0] = -sqrt(3./5);
  xi_h[1] = 0;
  xi_h[2] = sqrt(3./5);

  Kokkos::deep_copy(xi, xi_h);
  
  kk::View<double*> wi("wi",3);
  kk::View<double*>::HostMirror wi_h = kk::create_mirror(wi);
  
  wi_h[0] = 5./9;
  wi_h[1] = 8./9;
  wi_h[2] = 5./9;

  Kokkos::deep_copy(wi, wi_h);

  kk::View<double*>::HostMirror c0_h = kk::create_mirror(c0);
  kk::View<double*>::HostMirror c1_h = kk::create_mirror(c1);
  
  for(int i=0;i<nx;i++){
    double integ=0;
    for(size_t j=0;j<xi.size();j++){
      double x=i*dx+0.5*dx+xi_h[j]*0.5*dx;
      integ+=sin(2*M_PI*x/lx)*wi_h[j]*Legendre::Pn(0, xi_h[j]);
    }
    c0_h[i]=((2*0+1)/2.)*integ;

    integ=0;
    for(size_t j=0;j<xi.size();j++){
      double x=i*dx+0.5*dx+xi_h[j]*0.5*dx;
      integ+=sin(2*M_PI*x/lx)*wi_h[j]*Legendre::Pn(1, xi_h[j]);
    }
    c1_h[i]=((2*1+1)/2.)*integ;
  }

  Kokkos::deep_copy(c0, c0_h);
  Kokkos::deep_copy(c1, c1_h);

  kk::View<double*>::HostMirror u_h = kk::create_mirror(u);
  kk::View<double*>::HostMirror uFine_h = kk::create_mirror(uFine);
  
  for(int i=0;i<nx;i++) {
    uRef_h[i] = sin(2*M_PI/(nx-2)*(i+0.5));
  }
  
  for(int i=0;i<nx;i++) {
    u_h[i] = c0_h[i];
    for(int j=0;j<nInt;j++)
      uFine_h[i*nInt+j] = c0_h[i]+c1_h[i]*(-1+j*dInt);
  }
  
  
  // std::cout << "u = \n";
  // for(int i=0;i<nx;i++) 
  //   std::cout << i << "\t" << u[i] << std::endl;

  // exit(0);

  // std::string filename = "u.txt";
  // std::ofstream out(filename.c_str());
  // for(int i=0;i<nx;i++) 
  //   out << (i+0.5)*dx << "\t" << u_h[i] << "\t" << uRef_h[i] << std::endl;
  
  // out.close();
  
  // filename = "uFine.txt";
  // out.open(filename.c_str());
  // for(int i=0;i<nx*nInt;i++) 
  //   out << i*ddx << "\t" << uFine_h[i] << "\t" << uRefFine_h[i] << std::endl;
  
  // out.close();

  int nIter = round(T/dt); 
  //  nIter=100;
  std::cerr << "nIter = " << nIter << "\t" << T/dt << std::endl;

  // using scheduler_type = kk::TaskScheduler<Kokkos::DefaultExecutionSpace>;
  // auto scheduler = scheduler_type();
  
  auto start = std::chrono::system_clock::now();

  //kk::parallel_for("test", kk::RangePolicy<>(0, 0), KOKKOS_LAMBDA(int i) { c0[0] = -1; });

  //  kk::single(kk::TaskSingle(scheduler), [&]() { c0[0] = -1; });
  
  // kk::task_spawn(
  // 		 Kokkos::TaskSingle(scheduler),
  // 		 KOKKOS_LAMBDA() {c0[0] = -1;}
  // 		 );

  for(int iter = 0;iter<nIter;iter++) {

    kk::parallel_for("step1", kk::RangePolicy<>(1, nx-1), KOKKOS_LAMBDA(int i) {
	c0Tmp(i) = c0(i) - a*dt/2*(c0(i)+c1(i) - c0(i-1) - c1(i-1))/dx;
	c1Tmp(i) = c1(i) + 3.*a*dt/2/dx*(c0(i) - c1(i) - c0(i-1) - c1(i-1));
      });

    kk::parallel_for("boundary1", kk::RangePolicy<>(0, nx), KOKKOS_LAMBDA(int i) {
	if(i==0) {
	  c0Tmp(0) = c0Tmp(nx-2);
	  c0Tmp(nx-1) = c0Tmp(1);
	  c1Tmp(0) = c1Tmp(nx-2);
	  c1Tmp(nx-1) = c1Tmp(1);
	}
      });
    
    
    // c0Tmp(0) = c0Tmp(nx-2);
    // c0Tmp(nx-1) = c0Tmp(1);
    // c1Tmp(0) = c1Tmp(nx-2);
    // c1Tmp(nx-1) = c1Tmp(1);

    kk::parallel_for("step2", kk::RangePolicy<>(1, nx-1), KOKKOS_LAMBDA(int i) {
      c0New(i) = c0(i) - a*dt*(c0Tmp(i) + c1Tmp(i) - c0Tmp(i-1) - c1Tmp(i-1))/dx;
      c1New(i) = c1(i) + 3.*a*dt/dx*(c0Tmp(i) - c1Tmp(i) - c0Tmp(i-1) - c1Tmp(i-1));
      });

    kk::parallel_for("boundary2", kk::RangePolicy<>(0, nx), KOKKOS_LAMBDA(int i) {
	if(i==0) {
	  c0New(0) = c0New(nx-2);
	  c0New(nx-1) = c0New(1);
	  c1New(0) = c1New(nx-2);
	  c1New(nx-1) = c1New(1);
	}
      });

    // c0New(0) = c0New(nx-2);
    // c0New(nx-1) = c0New(1);
    // c1New(0) = c1New(nx-2);
    // c1New(nx-1) = c1New(1);

    kk::deep_copy(c0, c0New);
    kk::deep_copy(c1, c1New);
  }
  
  auto end = std::chrono::system_clock::now();

  Kokkos::deep_copy(c0_h, c0);
  Kokkos::deep_copy(c1_h, c1);
  
  std::chrono::duration<double> elapsed_seconds = end-start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  std::cout << "finished computation at " << std::ctime(&end_time)
            << "elapsed time: " << elapsed_seconds.count() << "s\n";

  
  for(int i=0;i<nx;i++) {
    uNew[i] = c0_h[i];
    for(int j=0;j<nInt;j++)
      uFineNew[i*nInt+j] = c0_h[i] + c1_h[i]*Legendre::Pn(1, -1+j*dInt);
  }

  double error = 0;
  for(int i=0;i<nx;i++) {
    error += (uNew[i]-uRef_h[i])*(uNew[i]-uRef_h[i])*dx;
  }
  error = sqrt(error);
  std::cerr << "error = " << error << std::endl;

  {
    std::string filename = "error.txt";
    std::ofstream out(filename.c_str(), std::ios::app);
    out << nx << "\t" << error << std::endl;
    out.close();
  }
  
  // filename = "uNew.txt";
  // out.open(filename.c_str());
  // for(int i=0;i<nx;i++) 
  //   out << (i+0.5)*dx << "\t" << uNew[i] << "\t" << uRef[i] << std::endl;
  
  // out.close();

  // filename = "uFineNew.txt";
  // out.open(filename.c_str());
  // for(int i=0;i<nx*nInt;i++) 
  //   out << i*ddx << "\t" << uFineNew[i] << "\t" << uRefFine[i] << std::endl;
  
  // out.close();
}
