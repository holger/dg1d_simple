cmake_minimum_required(VERSION 3.12)
project("Test_Kokkos")
enable_language(CXX)

include(FetchContent)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

FetchContent_Declare(Kokkos
  GIT_REPOSITORY https://github.com/kokkos/kokkos.git
  GIT_TAG aa1f48f3172069f212ed2c97b74b786999b26f17) # tags 4.0.00
FetchContent_MakeAvailable(Kokkos)

find_package(OpenMP REQUIRED)
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
set (Kokkos_ENABLE_OPENMP "On")

#if(NOT Kokkos_ENABLE_CUDA)
  add_executable(main_kokkos  main_kokkos.cpp)
  target_link_libraries(main_kokkos PUBLIC Kokkos::kokkos)
#endif()
