// g++ -o main main.cpp -O3 -fno-tree-vectorize

#include <CL/sycl.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <chrono>

#include "legendre.hpp"

int main(int argc, char** argv)
{
  std::cerr << "Test of the discontinious Galerkin method for 1d advection starts ...\n";

  if(argc < 2) {
    std::cerr << "ERROR: argc = " << argc << " < 2 !\n";
    std::cerr << "usage: ./cours <nx> \n";
    exit(1);
  }

  //default_selector device_selector;
  //  sycl::gpu_selector host_selector;
  //  sycl::cpu_selector device_selector;
  //host_selector device_selector;

  sycl::queue q(sycl::cpu_selector_v);
  std::cout << "Device: " << q.get_device().get_info<sycl::info::device::name>() << std::endl;
    
  int nx = atoi(argv[1]);
  double lx = 2*M_PI;
  double dx = lx/(nx-2);
  int nInt = 100;
  double dInt = 2./(nInt-1);
  double ddx = dx/(nInt-1);

  double cfl = 0.1;  // cfl = a*dt/dx
  double a = 1;
  double dt = cfl*dx/a;
  double T = lx/a;
  
  std::cerr << "nx = " << nx << std::endl;
  std::cerr << "lx = " << lx << std::endl;
  std::cerr << "dx = " << dx << std::endl;
  std::cerr << "cfl = " << cfl << std::endl;
  std::cerr << "a = " << a << std::endl;
  std::cerr << "dt = " << dt << std::endl;
  std::cerr << "T = " << T << std::endl;

  sycl::usm_allocator<int, sycl::usm::alloc::shared> q_alloc{q};

  typedef std::vector<double, sycl::usm_allocator<double, sycl::usm::alloc::shared>> usmVector;
  usmVector c0(nx,q_alloc);
  usmVector c1(nx,q_alloc);
  usmVector c0New(nx,q_alloc);
  usmVector c1New(nx,q_alloc);
  usmVector c0Tmp(nx,q_alloc);
  usmVector c1Tmp(nx,q_alloc);
  usmVector uNew(nx,q_alloc);
  usmVector u(nx,q_alloc);
  usmVector uFine(nx*nInt,q_alloc);
  usmVector uFineNew(nx*nInt,q_alloc);

  usmVector uRef(nx,q_alloc);
  usmVector uRefFine(nx*nInt,q_alloc);
  
  for(int i=0;i<nx*nInt;i++) {
    uRefFine[i] = sin(2*M_PI/((nx-2)*nInt)*(i+0.5));
  }

  usmVector xi({-sqrt(3./5), 0 ,sqrt(3./5)},q_alloc);
  usmVector wi({5./9, 8./9, 5./9},q_alloc);
    
  for(int i=0;i<nx;i++){
    double integ=0;
    for(size_t j=0;j<xi.size();j++){
      double x=i*dx+0.5*dx+xi[j]*0.5*dx;
      integ+=sin(2*M_PI*x/lx)*wi[j]*Legendre::Pn(0, xi[j]);
    }
    c0[i]=((2*0+1)/2.)*integ;

    integ=0;
    for(size_t j=0;j<xi.size();j++){
      double x=i*dx+0.5*dx+xi[j]*0.5*dx;
      integ+=sin(2*M_PI*x/lx)*wi[j]*Legendre::Pn(1, xi[j]);
    }
    c1[i]=((2*1+1)/2.)*integ;
  }
 
  for(int i=0;i<nx;i++) {
    uRef[i] = sin(2*M_PI/(nx-2)*(i+0.5));
  }
  
  for(int i=0;i<nx;i++) {
    u[i] = c0[i];
    for(int j=0;j<nInt;j++)
      uFine[i*nInt+j] = c0[i]+c1[i]*(-1+j*dInt);
  }
  
  
  // std::cout << "u = \n";
  // for(int i=0;i<nx;i++) 
  //   std::cout << i << "\t" << u[i] << std::endl;

  // exit(0);

  // std::string filename = "u.txt";
  // std::ofstream out(filename.c_str());
  // for(int i=0;i<nx;i++) 
  //   out << (i+0.5)*dx << "\t" << u[i] << "\t" << uRef[i] << std::endl;
  
  // out.close();
  
  // filename = "uFine.txt";
  // out.open(filename.c_str());
  // for(int i=0;i<nx*nInt;i++) 
  //   out << i*ddx << "\t" << uFine[i] << "\t" << uRefFine[i] << std::endl;
  
  // out.close();

  int nIter = round(T/dt);
  //  nIter=100;
  std::cerr << "nIter = " << nIter << "\t" << T/dt << std::endl;

  auto start = std::chrono::system_clock::now();

  double *pc0 = &c0[0];
  double *pc1 = &c1[0];
  double *pc0Tmp = &c0Tmp[0];
  double *pc1Tmp = &c1Tmp[0];
  double *pc0New = &c0New[0];
  double *pc1New = &c1New[0];
  
  for(int iter = 0;iter<nIter;iter++) {
    q.parallel_for(sycl::range<1>(nx-2), [=] (sycl::id<1> j){
      int i=j+1;
      pc0Tmp[i] = pc0[i] - a*dt/2*(pc0[i]+pc1[i] - pc0[i-1] - pc1[i-1])/dx;
      pc1Tmp[i] = pc1[i] + 3.*a*dt/2/dx*(pc0[i] - pc1[i] - pc0[i-1] - pc1[i-1]);
      
    }).wait();

    q.single_task([=] (){
      pc0Tmp[0] = pc0Tmp[nx-2];
      pc0Tmp[nx-1] = pc0Tmp[1];
      pc1Tmp[0] = pc1Tmp[nx-2];
      pc1Tmp[nx-1] = pc1Tmp[1];
    }).wait();
      
    q.parallel_for(sycl::range<1>(nx-2), [=] (sycl::id<1> j){
      int i=j+1;
      pc0New[i] = pc0[i] - a*dt*(pc0Tmp[i] + pc1Tmp[i] - pc0Tmp[i-1] - pc1Tmp[i-1])/dx;
      pc1New[i] = pc1[i] + 3.*a*dt/dx*(pc0Tmp[i] - pc1Tmp[i] - pc0Tmp[i-1] - pc1Tmp[i-1]);

    }).wait();

    q.single_task([=] (){
      pc0New[0] = pc0New[nx-2];
      pc0New[nx-1] = pc0New[1];
      pc1New[0] = pc1New[nx-2];
      pc1New[nx-1] = pc1New[1];
    }).wait();

    q.parallel_for(sycl::range<1>(nx), [=] (sycl::id<1> i){
      pc0[i] = pc0New[i];
      pc1[i] = pc1New[i];
    }).wait();
  }
  
  auto end = std::chrono::system_clock::now();

  std::chrono::duration<double> elapsed_seconds = end-start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  std::cout << "finished computation at " << std::ctime(&end_time)
            << "elapsed time: " << elapsed_seconds.count() << "s\n";

  
  for(int i=0;i<nx;i++) {
    uNew[i] = c0New[i];
    for(int j=0;j<nInt;j++)
      uFineNew[i*nInt+j] = c0New[i] + c1New[i]*Legendre::Pn(1, -1+j*dInt);
  }

  double error = 0;
  for(int i=0;i<nx;i++) {
    error += (uNew[i]-uRef[i])*(uNew[i]-uRef[i])*dx;
  }
  error = sqrt(error);
  std::cerr << "error = " << error << std::endl;

  {
    std::string filename = "error.txt";
    std::ofstream out(filename.c_str(), std::ios::app);
    out << nx << "\t" << error << std::endl;
    out.close();
  }
  
  // filename = "uNew.txt";
  // out.open(filename.c_str());
  // for(int i=0;i<nx;i++) 
  //   out << (i+0.5)*dx << "\t" << uNew[i] << "\t" << uRef[i] << std::endl;
  
  // out.close();

  // filename = "uFineNew.txt";
  // out.open(filename.c_str());
  // for(int i=0;i<nx*nInt;i++) 
  //   out << i*ddx << "\t" << uFineNew[i] << "\t" << uRefFine[i] << std::endl;
  
  // out.close();
}
